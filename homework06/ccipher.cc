#include <string>
#include <iostream>
#include <algorithm>
#include "ccipher.h"


// -------------------------------------------------------
// Caesar Cipher implementation

struct Cipher::CipherCheshire {
   string cipher_alpha; 
};

CCipher::CCipher() {    
    string cipher_alpha = "abcdefghijklmnopqrstuvwxyz";
    rotate_string(cipher_alpha, 0);
    smile->cipher_alpha = cipher_alpha;
}

CCipher::CCipher(int rot) {
    if (0>rot) {
    	cerr << "Error: Caesar cipher is less than 0" << endl;
	exit(EXIT_FAILURE);
    }
    string cipher_alpha = "abcdefghijklmnopqrstuvwxyz";
    rotate_string(cipher_alpha, rot);
    smile->cipher_alpha = cipher_alpha;
}

CCipher::~CCipher() {
}
// -------------------------------------------------------

// Rotates the input string in_str by rot positions
void rotate_string(string& in_str, int rot)
{
    while (rot) {
    	in_str[ALPHABET_SIZE] = in_str[0];
	for (unsigned int x=0;ALPHABET_SIZE>x; x++) {
	    in_str[x] = in_str[x+1];
	}
	rot--;
    }
}
