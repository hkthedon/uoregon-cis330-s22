#include "cipher.h"
struct Cipher::CipherCheshire {
    string cipher_alpha;
};

bool is_valid_alpha(string alpha);


Cipher::Cipher()
{
    smile = new CipherCheshire;
    smile->cipher_alpha = "abcdefghijklmnopqrstuvwxyz"; 
}

Cipher::Cipher(string cipher_alpha)
{
    if(!is_valid_alpha(cipher_alpha)){
	    cerr << "Invalid cipher alphabet/key: " << cipher_alpha << endl;
    	    exit(EXIT_FAILURE);
    }
    
    smile = new CipherCheshire;
    smile->cipher_alpha = cipher_alpha;
}

Cipher::~Cipher()
{
    delete smile;
}

string Cipher::encrypt(string raw)
{
    cout << "Encrypting...";
    string retStr;
    for(unsigned int x = 0; x < raw.size(); x++){
	if(raw[x] == ' '){ retStr.push_back(' '); }
	else{
    		unsigned int position = find_pos("abcdefghijklmnopqrstuvwxyz", LOWER_CASE(raw[x]));
		
		if(raw[x] == UPPER_CASE(raw[x])){
		       	retStr.push_back(UPPER_CASE(smile->cipher_alpha[position]));
	       	}
		
		else if(raw[x] == LOWER_CASE(raw[x])){
		       	retStr.push_back(LOWER_CASE(smile->cipher_alpha[position]));
	       	}
	}
    }

    cout << "Done" << endl;
    return retStr;
}


string Cipher::decrypt(string enc)
{
    string retStr;
    cout << "Decrypting...";
    string alphabet = "abcdefghijklmnopqrstuvwxyz";

    for(unsigned int x = 0; x < enc.size(); x++){
    	if(enc[x] == ' '){ retStr.push_back(' '); }
	else{
		unsigned int position = find_pos(smile->cipher_alpha, LOWER_CASE(enc[x]));
		if(enc[x] == UPPER_CASE(enc[x])){
		       	retStr.push_back(UPPER_CASE(alphabet[position]));
	       	}
		else if(enc[x] == LOWER_CASE(enc[x])){
		       	retStr.push_back(LOWER_CASE(alphabet[position]));
	       	}
	}
    }
    cout << "Done" << endl;
    return retStr;
}

unsigned int find_pos(string alpha, char c)
{
    unsigned int pos = 0;
    for(unsigned int x = 0; x < alpha.size(); x++){
    	if(alpha[x] == c){
             pos = x;
	}
    }
    return pos;
}

bool is_valid_alpha(string alpha)
{
    bool validity = true;

    if(alpha.size() != ALPHABET_SIZE) {
        validity = false; 
    } 
    else {
        unsigned int letter_exists[ALPHABET_SIZE];

        for(unsigned int x = 0; x < ALPHABET_SIZE; x++) {
            letter_exists[x] = 0;
        }

        for(unsigned int x = 0; x < alpha.size(); x++) {
            char c = alpha[x];
            if(!((c >= 'a') && (c <= 'z'))) {
                validity = false;
            } else {
                letter_exists[(c - 'a')]++;
            }
        }

        for(unsigned int x = 0; x < ALPHABET_SIZE; x++) {
            if(letter_exists[x] != 1) {
                validity = false;
            }
        }

    }

    return validity;
}

