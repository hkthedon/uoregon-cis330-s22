#include "rbt.h"

// ---------------------------------------
// Node class
// Default constructor
RBTNode::RBTNode() : Node() {
    color = BLACK;
}

// Constructor
RBTNode::RBTNode(int in) : Node(in) {
    color = BLACK;
}
// Destructor
RBTNode::~RBTNode() {
}

void RBTNode::add_color(Node_color c)
{
  color = c;
}

void RBTNode::print_color(ostream& to)
{
    if(color == RED) {
        to << "Red";
    } else if (color == BLACK) {
        to << "Black";
    } else {
        cerr << "ERR: invalid color" << endl;
    }
}
void RBTNode::print_info(ostream& to)
{
    to << get_key() << " ";
    print_color(to);
}

Node_color RBTNode::get_color()
{
  return color;
}
// ---------------------------------------


// ---------------------------------------
// RBT class
// Constructor and destructor
RBT::RBT() : BST()
{
  sentinel = new RBTNode(-1);
  root = sentinel;
}
RBT::~RBT()
{
    // re-using BST's inorder_free
    inorder_free(root, sentinel);
    // This makes sure that root is set to nullptr, so that the parent class's
    // constructor does not try to free the tree again
    root = nullptr;
    delete sentinel; 
}

// Functions that are basically wrappers for the parent class functions
// minimum key in the BST
RBTNode* RBT::tree_min()
{
    // return (RBTNode*) get_min(root);
    // return (RBTNode*) BST::tree_min();
    return (RBTNode*) BST::get_min(root, sentinel);
}
// maximum key in the BST
RBTNode* RBT::tree_max()
{
    // return (RBTNode*) get_max(root);
    // return (RBTNode*) BST::tree_max();
    return (RBTNode*) BST::get_max(root, sentinel);
}

// Get successor of the given node
RBTNode* RBT::get_succ(RBTNode* in)
{
  return (RBTNode*) BST::get_succ((Node*) in, sentinel);
}

// Get predecessor of the given node
RBTNode* RBT::get_pred(RBTNode* in)
{
  return (RBTNode*) BST::get_pred((Node*) in, sentinel);
}

// Search the tree for a given key
RBTNode* RBT::tree_search(int search_key)
{
  return (RBTNode*) BST::tree_search(search_key, sentinel);
}

void RBT::walk(ostream& to)
{
  BST::inorder_walk(root, to, sentinel);
}



// New functions to RBT
// right rotate

void RBT::right_rotate(RBTNode* y)
{

	RBTNode* in =(RBTNode*)(y->get_left());
	y->add_left(in->get_right());
	if (sentinel != in->get_right())
		in->get_right()->add_parent(y);
	in->add_parent(y->get_parent());

	if (sentinel == y->get_parent())
		root = in;
	else if (y->get_parent()->get_right() == y)
		y->get_parent()->add_right(in);
	else
		y->get_parent()->add_left(in);
	in->add_right(y);
	y->add_parent(in);

}

// Left rotate
void RBT::left_rotate(RBTNode* x)
{

	RBTNode* in =(RBTNode*)(x->get_right());
	x->add_right(in->get_left());

	if (sentinel != in->get_left())
		in->get_left()->add_parent(x);
	in->add_parent(x->get_parent());

	if (sentinel == x->get_parent())
		root = in;
	else if (x->get_parent()->get_left()==x)
		x->get_parent()->add_left(in);
	else
		x->get_parent()->add_right(in);
	in->add_left(x);
	x->add_parent(in);

}
void RBT::rb_insert_fixup(RBTNode* in)
{
	RBTNode* y = nullptr;
	RBTNode* parent = (RBTNode*)(in->get_parent());
	RBTNode* parentget = (RBTNode*)(parent->get_parent());
	RBTNode* RBTparent = (RBTNode*)(in->get_parent()->get_parent());
	RBTNode* RBTroot = (RBTNode*)(root);

	while (RED == (parent->get_color())) {
		if (in->get_parent() == in->get_parent()->get_parent()->get_left()) {
			y =(RBTNode*)(in->get_parent()->get_parent()->get_right());
			if (RED == y->get_color()) {
				parent->add_color(BLACK);
				y->add_color(BLACK);
				parentget->add_color(RED);
				in = (RBTNode*)(in->get_parent()->get_parent());
			}
			else {
				if (in->get_parent()->get_right() == in) {
					in = (RBTNode*)(in->get_parent());
					left_rotate(in);
				}
				parent->add_color(BLACK);
				RBTparent->add_color(RED);
				right_rotate((RBTNode*)in->get_parent()->get_parent());
			}
		}
		else {
			y = (RBTNode*)(in->get_parent()->get_parent()->get_left());
			if (y->get_color() == RED) {
				parent->add_color(BLACK);
				y->add_color(BLACK);
				RBTparent->add_color(RED);
				in =(RBTNode*)(in->get_parent()->get_parent());
			}
			else {
				if (in == in->get_parent()->get_left()) {
					in = (RBTNode*)(in->get_parent());
					right_rotate(in);
				}
				parent->add_color(BLACK);
				RBTparent->add_color(RED);
				left_rotate((RBTNode*)(in->get_parent()->get_parent()));
			}
		}
	}
	RBTroot->add_color(BLACK);
}


void RBT::rb_insert_node(RBTNode* in)
{
	RBTNode *y = (RBTNode *)sentinel;
	RBTNode *x = (RBTNode *)root;
	while (sentinel != x) {
		y = x;
		if (x->get_key()>in->get_key()) {
			x = (RBTNode *)x->get_left();
		}
		else {
			x = (RBTNode *)x->get_right();
		}
	}
	in->add_parent(y);
	if (sentinel==y) {
		root = in;
	}
	else if (y->get_key()>in->get_key()) {
		y->add_left(in);
	}
	else {
		y->add_right(in);
	}
	in->add_left(sentinel);
	in->add_right(sentinel);
	in->add_color(RED);
	rb_insert_fixup(in);
}

void RBT::rb_delete_fixup(RBTNode* in)
{
	/* TODO */
}

void RBT::rb_delete_node(RBTNode* out)
{
	/*I was not going to include this but I thought I might be able to get some partial extra credit, so I might as well push it out with insert, I did not have time to finish and work through it all, and my fixup was too messy it didn't compile*/
	RBTNode* x = nullptr;
	RBTNode* y = out;
	Node_color colory = y->get_color();
	if (sentinel == out->get_left()) {
		x = (RBTNode*)out->get_right();
		if (out->get_parent() == sentinel) {
        		root = out->get_right();
    		}
                else if (out == out->get_parent()->get_left()) {
                        out->get_parent()->add_left(out->get_right());
    		}
                else
    		{
                        out->get_parent()->add_right(out->get_right());
    		}
        	(out->get_right())->add_parent(out->get_parent());
	}
	else if (sentinel == out->get_right()) {
		x = (RBTNode*)out->get_left();
		if (out->get_parent() == sentinel) {
        		root = out->get_left();
    		}
                else if (out == out->get_parent()->get_left()) {
                        out->get_parent()->add_left(out->get_left());
    		}
                else
    		{
                        out->get_parent()->add_right(out->get_left());
    		}
                (out->get_left())->add_parent(out->get_parent());
	}
	else {
		y = (RBTNode*)get_min(out->get_right());
		colory = y->get_color();
		x = (RBTNode*)y->get_right();
		if (out != y->get_parent()) {
			if (y->get_parent() == sentinel) {
            			root = y->get_right();
        		}
        		else if (y == y->get_parent()->get_left()) {
            			y->get_parent()->add_left(y->get_right());
        		}
        		else {
            			y->get_parent()->add_right(y->get_right());
        		}
            		(y->get_right())->add_parent(y->get_parent());
			y->add_right(out->get_right());
			y->get_right()->add_parent(y);
		}
		else {
			x->add_parent(y);
		}
		if (sentinel == out->get_parent()) {
        		root = y;
    		}
     		else if (out->get_parent()->get_left() == out) {
                        out->get_parent()->add_left(out);
    		}
                else {
                        out->get_parent()->add_right(out);
    		}
                y->add_parent(out->get_parent());
		y->add_left(out->get_left());
		y->get_left()->add_parent(y);
		y->add_color(out->get_color());
	}
	if (BLACK == colory)
		rb_delete_fixup(x);
	delete out;
}	
// ---------------------------------------
