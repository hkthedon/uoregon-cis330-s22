/*
  Implementation of methods for classes Response, AngryResponse, HappyResponse
*/
#include <iostream>
#include <string>
#include <algorithm>

#include "response.h"

using namespace std;

/*
  Implementation of Word class
  Don't worry too hard about these implementations.
  If you'd like to learn more about STL see: 
    https://www.geeksforgeeks.org/the-c-standard-template-library-stl/
*/
string Word::upper()
{
  string result(theWord);
  transform(result.begin(), result.end(), result.begin(), ::toupper);
  return result;
}

string Word::lower()
{
  string result(theWord);
  transform(result.begin(), result.end(), result.begin(), ::tolower);
  return result;
}

/*
  Implementation of Response methods
*/
bool Response::checkAndRespond(const string& inWord, ostream& toWhere)
{
    if (string::npos != inWord.find(keyword.upper())){
    	respond(toWhere);
	return true;
    }
    return false;
}

void Response::respond(ostream& toWhere)
{
    (toWhere << "I am neither angry nor happy: " << response << endl);
}


void AngryResponse::respond(ostream& toWhere)
{
    (toWhere << "I am angry: " << response << endl);
}

void HappyResponse::respond(ostream& toWhere)
{
    (toWhere << "I am happy: " << response << endl);
}
