#include <string>
#include <iostream>
#include <vector>
#include "kcipher.h"



// Running Key Cipher implementation

struct Cipher::CipherCheshire {
    string cipher_alpha;
};

struct KCipher::KKCheshire {
    vector<string> book;
    unsigned int page = 0;
};
// helper funcs

bool validity(string page) {
    bool valid = true;
    if (0==page.length()) {
        cout << "Invalid Running key: " << page << endl;
        valid = false;
        return valid;
    }

    for (unsigned int x = 0; page.length()>x; x++) {
        char c = page[x];
        if (!(((c >= 'a') && (c <= 'z')) || (c == ' '))) {
            cout << "Invalid Running key: " << page << endl;
            valid = false;
            return valid;
        }
    }

    return valid;
}

void spaces_rem(string& str) {
    
    for (unsigned int x = 0; x < str.length(); x++) {
        if (str[x] == ' ')
            str.erase(str.begin() + x);
    }
}


KCipher::KCipher() {
    vector<string> book;
    string page;
    for (unsigned int x = 0;MAX_LENGTH>x; x++) {
    	page =page+'a';
    }
    
    book.push_back(page);
    kksmile = new KKCheshire;
    kksmile->book = book;
};


KCipher::KCipher(string page_one) {
    if (!validity(page_one)) {
    	exit(EXIT_FAILURE);
    }
    
    kksmile = new KKCheshire;
    kksmile->book.push_back(page_one);
};


void KCipher::add_key(string page) {
    if (!validity(page)) {
    	exit(EXIT_FAILURE);
    }
    
    kksmile->book.push_back(page);
};


void KCipher::set_id(unsigned int n) {
    if (n > kksmile->book.size()) {
    	cout << "Warning: invalid id: " << n << endl;
	exit(EXIT_FAILURE);
    }
    
    kksmile->page = n;
};


string KCipher::encrypt(string raw) {
    string retStr;
    spaces_rem(kksmile->book[kksmile->page]);
    string enc_page = kksmile->book[kksmile->page];
    vector<int> space_pos;
    int lenwspace = raw.length();

    for (int x = 0; x<lenwspace; x++) {
    	if (raw[x] == ' ')
	    space_pos.push_back(x);
    }
    
    spaces_rem(raw);
    
    if (enc_page.length() < raw.length()) {
    	cout << "Invalid Running key: " << enc_page << endl;
	exit(EXIT_FAILURE);
    }
    
    cout << "Encrypting...";
    int lenr = raw.length();
    
    for (int x = 0; x < lenr; x++) {
    	char lowerr = LOWER_CASE(raw[x]);
	char lowere = LOWER_CASE(enc_page[x]);

	int posr = find_pos(smile->cipher_alpha, lowerr);
	int pose = find_pos(smile->cipher_alpha, lowere);
	
	rotate_string(smile->cipher_alpha, posr);
	char ciph = smile->cipher_alpha[pose];

	if (isupper(raw[x]))
	    ciph = UPPER_CASE(smile->cipher_alpha[pose]);
	retStr += ciph;
	rotate_string(smile->cipher_alpha, ALPHABET_SIZE - posr);
    }
    
    int vecl = space_pos.size();
    
    for (int x = 0; x < vecl; x++) {
	   retStr.insert(space_pos[x], " "); 
    }
    
    cout << "Done" << endl;
    return retStr;
}


string KCipher::decrypt(string enc) {
    cout << "Decrypting...";
    string retStr;
    string enc_page = kksmile->book[kksmile->page];
    vector<int> space_pos;
    int lenespace = enc.length();
    
    for (int x = 0; x < lenespace; x++) {
    	if (enc[x] == ' ')
	    space_pos.push_back(x);
    }
    spaces_rem(enc);
    int lene = enc.length();
    for (int x = 0; x < lene; x++) {
        char lowere = LOWER_CASE(enc[x]);
        char lowerp = LOWER_CASE(enc_page[x]);
        int posp = find_pos(smile->cipher_alpha, lowerp);
        rotate_string(smile->cipher_alpha, posp);
        int posd = find_pos(smile->cipher_alpha, lowere);
	rotate_string(smile->cipher_alpha, ALPHABET_SIZE - posp);
        char ciph = smile->cipher_alpha[posd];

        if (isupper(enc[x]))
	    ciph = UPPER_CASE(smile->cipher_alpha[posd]);
        retStr += ciph;
    }

    int vecl = space_pos.size();
    for (int x = 0; x < vecl; x++) {
	   retStr.insert(space_pos[x], " "); 
    }
    
    cout << "Done" << endl;
    return retStr;
}


// Destructor 
KCipher::~KCipher() {
    delete kksmile;
};

// -------------------------------------------------------
