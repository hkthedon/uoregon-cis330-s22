#include <string>
#include <iostream>
#include <vector>
#include "kcipher.h"
#include "vcipher.h"

struct Cipher::CipherCheshire {
    string cipher_alpha = "abcdefghijklmnopqrstuvwxyz";
};

struct KCipher::KKCheshire {
    vector<string> book;
    unsigned int page = 0;
};

VCipher::VCipher() : KCipher() {
};

VCipher::VCipher(string key) {
    for (unsigned int x = 0; x < key.size(); x++) {
    	char c = key[x];
	if (!(c >= 'a') && (c <= 'z')) {
	    cout << "Error: not a valid Vigenere key word" << endl;
	    exit(EXIT_FAILURE);
	}
    }

    string page;
    while (page.size() < MAX_LENGTH) {
    	page=page+key;
    }
    kksmile->book.clear();
    kksmile->book.push_back(page);
};

VCipher::~VCipher() {
};
